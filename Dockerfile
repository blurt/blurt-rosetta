# This is a Rosetta-flavored dockerfile that complies with all Rosetta
# [recommendations](https://www.rosetta-api.org/docs/node_deployment.html#dockerfile-expectations) for Dockerfiles
#
# * Build Anywhere
# * Compile exclusively from source
FROM registry.gitlab.com/blurt/blurt-rosetta AS builder

# clone blurt and get submodules, build as rpc node
RUN git clone --recursive https://gitlab.com/blurt/blurt.git && \
    cd blurt && \
    git checkout dev
RUN cd blurt && mkdir build && cd build && \
    conan install .. -s compiler=gcc -s compiler.libcxx=libstdc++11 -if=. -pr=default --build=missing
RUN cd blurt/build && \
    cmake -DBLURT_STATIC_BUILD=ON -DLOW_MEMORY_NODE=OFF -DCLEAR_VOTES=OFF -DBUILD_BLURT_TESTNET=OFF \
    -DSKIP_BY_TX_ID=OFF -DBLURT_LINT_LEVEL=OFF -DENABLE_MIRA=OFF -DCMAKE_BUILD_TYPE=Release ..
RUN cd blurt/build && \
    make -j$(nproc) blurtd
RUN cp ./blurt/build/bin/blurtd /usr/bin/blurtd

# install Go
RUN wget https://golang.org/dl/go1.14.6.linux-amd64.tar.gz && \
    tar xvf go1.14.6.linux-amd64.tar.gz
ENV PATH=/go/bin:$PATH
ENV GOROOT=/go

# get the required genesis snapshot to start the chain
RUN wget -O /snapshot.json  https://cloudflare-ipfs.com/ipfs/QmPrwVpwe4Ya46CN9LXNnrUdWvaDLMwFetMUdpcdpjFbyu 1>/dev/null

# clone and build the rosetta middleware
ADD "https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h" skipcache
RUN git clone https://gitlab.com/blurt/blurt-rosetta.git && cd blurt-rosetta && \
    go build -x -v -a

# deployment image setup - this eliminates the working files for the build from the image
FROM ubuntu:eoan

EXPOSE 8090
EXPOSE 8091
EXPOSE 1776

COPY --from=builder /usr/bin/blurtd /usr/bin/blurtd
COPY --from=builder /blurt-rosetta/blurt-rosetta /usr/bin/blurt-rosetta
RUN mkdir /data
COPY --from=builder /snapshot.json /data/

CMD ["/usr/bin/blurt-rosetta", "/usr/bin/blurtd", "http://localhost:8091", "/data"]

