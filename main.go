package main

import (
	"context"
	"fmt"
	"gitlab.com/blurt/blurt-rosetta/pkg/svc"
	"gitlab.com/stalker.loki/disrupt"
	"gitlab.com/stalker.loki/slog"
	"os"
	"os/exec"
	"strings"
)

var (
	version                       = "v0.0.1"
	endpoint, dataDir, binaryPath string
	executablePath                = os.Args[0]
	blurtCmd                      *exec.Cmd
	exitCode                      int
)

func main() {
	switch {
	case len(os.Args) == 1:
		// if no parameters are given, the RPC server is assumed to be the default
		// and is already running
		endpoint = "localhost:8091"
		runRosetta()
	case len(os.Args) == 2 && os.Args[1] == "help":
		// If only one parameter is given, and it is "help" print out the help text
		printHelp()
	case len(os.Args) == 2:
		// If only one parameter is given, assume it is the RPC endpoint address and
		// it is already running
		endpoint = os.Args[1]
		runRosetta()
	case len(os.Args) == 4:
		// first arg is blurt's http endpoint,
		// second is the data directory,
		// third is the blurtd binary path
		endpoint = os.Args[1]
		dataDir = os.Args[2]
		binaryPath = os.Args[3]
		args := []string{"--data-dir", dataDir, "--webserver-http-endpoint", endpoint}
		slog.Debug("launch args:", binaryPath, strings.Join(args, " "))
		blurtCmd = exec.Command(binaryPath, args...)
		blurtCmd.Stdout = os.Stdout
		blurtCmd.Stderr = os.Stderr
		blurtCmd.Stdin = nil
		if err := blurtCmd.Start(); err != nil {
			slog.Error("error launching blurtd:", err)
			Fail()
		}
		slog.Info("starting up blurtd...")
		go func() {
			// if we have started up the server, if it goes down, shut down the rosetta
			// RPC because it has no endpoint to communicate with
			if err := blurtCmd.Wait(); slog.Check(err) {
				slog.Error("error waiting for blurtd to stop running", err)
				Fail()
			} else {
				slog.Info("endpoint has shut down, terminating Rosetta API endpoint")
				_ = blurtCmd.Process.Kill()
				disrupt.Request()
			}
		}()
		runRosetta()
	default:
		fmt.Println("given parameters are incorrect, printing help")
		fmt.Println()
		printHelp()
		Fail()
	}
	<-disrupt.HandlersDone
	os.Exit(exitCode)
}

func Fail() {
	exitCode = 1
	disrupt.Request()
}

func printHelp() {
	fmt.Print(`blurt_rosetta ` + version + `

	Coinbase Rosetta middleware to allow access to a Blurt RPC daemon through Rosetta API

Usage:

	` + executablePath + `

		Run rosetta_blurt with default local endpoint assumed to be running on default 
		address: http://localhost:8091
	
	` + executablePath + ` <blurtd http endpoint address>
	
		Run rosetta_blurt with default endpoint assumed to be running on the given address
	
	` + executablePath + ` <blurtd http endpoint> <blurtd data dir> <blurtd binary path>

		Launch blurt RPC node with the given parameters and attach to it
	
`)
	os.Exit(0)
}

func runRosetta() {
	endpoint = "http://" + endpoint
	slog.Info("Running Rosetta API Endpoint for Blurt")
	disrupt.AddHandler(cleanupRosetta)
	var server *svc.Server
	var err error
	// create context and attach cancel to disrupt handler chain so the server is stopped
	// by doing this disrupt becomes the master kill switch
	var ctx context.Context
	var cancel func()
	ctx, cancel = context.WithCancel(context.Background())
	_ = cancel
	// Create a new server
	if server, err = svc.New(ctx, endpoint); slog.Check(err) {
		disrupt.Request()
	} else {
		// run service until its context is cancelled
		if err = server.Run(); slog.Check(err) {
		}
		disrupt.AddHandler(func() {
			slog.Debug("stopping blurt-rosetta endpoint")
		})
	}
}

func cleanupRosetta() {
	exitCode = 0
	slog.Info("shutting down blurt-rosetta")
}
