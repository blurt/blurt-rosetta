module gitlab.com/blurt/blurt-rosetta

go 1.14

require (
	github.com/btcsuite/btcd v0.20.1-beta
	github.com/btcsuite/btcutil v0.0.0-20190425235716-9e5f4b9a998d
	github.com/coinbase/rosetta-sdk-go v0.3.3
	github.com/davecgh/go-spew v1.1.1
	github.com/ethereum/go-ethereum v1.9.15
	github.com/kr/pretty v0.2.0 // indirect
	github.com/pkg/errors v0.9.1
	gitlab.com/stalker.loki/disrupt v1.0.8
	gitlab.com/stalker.loki/slog v0.0.3
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de
)
