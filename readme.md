 ![](assets/rosetta-stone.svg) 
# blurt-rosetta

Implementation of the [Coinbase](https://www.coinbase.com) [Rosetta](https://www.rosetta-api.org/)
API for an adapter middleware for the Blurt Blockchain

---

Rosetta is an initiative and API specification created by [Coinbase](https://www.coinbase.com) 
aiming at reducing the complexity of implementing secure and compliant access for exchanges
and wallets with sufficient flexibility that the nuances of a blockchain's specifics are not
inaccessible.

This repository contains a fork of 
[https://github.com/asuleymanov/steem-go](https://github.com/asuleymanov/steem-go) 
which will be at first updated/modified only to achieve compliance with the 
[Rosetta](https://www.rosetta-api.org) specification, but eventually form the basis of the 
canonical [Go](https://golang.org) RPC when the repository is compliant and is integrated into the
main [Blurt](https://gitlab.com/blurt/blurt) repository.

The launch CLI is capable of spawning and controlling its own instance of `blurtd`, connecting to a
a default configured local instance, or given an arbitrary remote `blurtd` endpoint to connect to.

---

Logo image made by [Smashicons](https://www.flaticon.com/authors/smashicons) from [www.flaticon.com](https://www.flaticon.com)
