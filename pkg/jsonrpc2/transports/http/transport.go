package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/stalker.loki/slog"
	"io/ioutil"
	"net/http"
	"sync"
	"time"

	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/types"
)

type Transport struct {
	Url       string
	client    http.Client
	requestID uint64
	reqMutex  sync.Mutex
}

func NewTransport(url string) (*Transport, error) {
	timeout := time.Duration(5 * time.Second)
	return &Transport{
		client: http.Client{Timeout: timeout},
		Url:    url,
	}, check(url)
}

func (caller *Transport) Call(method string, args []interface{}, reply interface{}) (err error) {
	caller.reqMutex.Lock()
	defer caller.reqMutex.Unlock()
	// increase request id
	// TODO: is this really necessary since the next addition will roll it to zero? Not to mention that it would take
	//  decades for this to be running before it gets anywhere near it, thus, I am removing it
	//if caller.requestID == math.MaxUint64 {
	//	caller.requestID = 0
	//}
	caller.requestID++
	request := types.RPCRequest{
		Method: method,
		JSON:   "2.0",
		ID:     caller.requestID,
		Params: args,
	}
	var reqBody []byte
	if reqBody, err = json.Marshal(request); slog.Check(err) {
		return
	}
	var resp *http.Response
	if resp, err = caller.client.Post(caller.Url, "application/json", bytes.NewBuffer(reqBody)); slog.Check(err) {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("unexpected status code: %d", resp.StatusCode)
		return
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	if slog.Check(err) {
		err = errors.Wrap(err, "failed to read body")
		return
	}
	rpcResponse := &types.RPCResponse{}
	if err = json.Unmarshal(respBody, rpcResponse); slog.Check(err) {
		err = errors.Wrapf(err, "failed to unmarshal response: %+v", string(respBody))
		return
	}
	//slog.Debug(spew.Sdump(*rpcResponse))
	if rpcResponse.Error != nil {
		err = rpcResponse.Error
		return
	}
	if rpcResponse.Result != nil {
		if err = json.Unmarshal(*rpcResponse.Result, reply); slog.Check(err) {
			err = errors.Wrapf(err, "failed to unmarshal rpc result: %+v", string(*rpcResponse.Result))
			return
		}
	}
	return
}

func (caller *Transport) SetCallback(api string, method string, notice func(args json.RawMessage)) error {
	panic("not supported")
}

func (caller *Transport) Close() error {
	return nil
}

func check(url string) (err error) {
	var resp *http.Response
	if resp, err = http.Get(url); slog.Check(err) {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return errors.New(fmt.Sprintf("Error. URL: %s STATUS: %v\n", url, resp.StatusCode))
	}
	return
}
