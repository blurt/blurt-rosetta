package types

import (
	"encoding/json"
	"gitlab.com/stalker.loki/slog"
	"strconv"
	"strings"

	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/encoding/transaction"
)

// Asset type from parameter JSON
type Asset struct {
	Amount float64
	Symbol string
}

// UnmarshalJSON unpacking the JSON parameter in the Asset type.
func (op *Asset) UnmarshalJSON(data []byte) (err error) {
	var str string
	if str, err = strconv.Unquote(string(data)); slog.Check(err) {
		return
	}
	param := strings.Split(str, " ")
	var s float64
	if s, err = strconv.ParseFloat(param[0], 64); slog.Check(err) {
		return
	}
	op.Amount = s
	op.Symbol = param[1]
	return
}

// MarshalJSON function for packing the Asset type in JSON.
func (op *Asset) MarshalJSON() ([]byte, error) {
	return json.Marshal(op.String())
}

// MarshalTransaction is a function of converting type Asset to bytes.
func (op *Asset) MarshalTransaction(encoder *transaction.Encoder) (err error) {
	var ans []byte
	if ans, err = json.Marshal(op); slog.Check(err) {
		return
	}
	var str string
	if str, err = strconv.Unquote(string(ans)); slog.Check(err) {
		return
	}
	return encoder.EncodeMoney(str)
}

// String function convert type Asset to string.
func (op *Asset) String() string {
	var out string
	if op.Symbol != "SHARES" {
		out = strconv.FormatFloat(op.Amount, 'f', 3, 64)
	} else {
		out = strconv.FormatFloat(op.Amount, 'f', 6, 64)
	}
	return out + " " + op.Symbol
}

// StringAmount function convert type Asset.Amount to string.
func (op *Asset) StringAmount() string {
	return strconv.FormatFloat(op.Amount, 'f', 3, 64)
}
