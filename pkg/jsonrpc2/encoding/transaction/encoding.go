package transaction

// TransactionMarshaller interface for converting data into byte
type Marshaller interface {
	MarshalTransaction(*Encoder) error
}
