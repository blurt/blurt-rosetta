package wif

import (
	"github.com/btcsuite/btcutil"
	"github.com/pkg/errors"
	"gitlab.com/stalker.loki/slog"
)

// Decode can be used to turn WIF into a raw private key (32 bytes).
func Decode(wif string) (out []byte, err error) {
	var w *btcutil.WIF
	w, err = btcutil.DecodeWIF(wif)
	if slog.Check(err) {
		return nil, errors.Wrap(err, "failed to decode WIF")
	}
	out = w.PrivKey.Serialize()
	return
}

// GetPublicKey returns the public key associated with the given WIF
// in the 33-byte compressed format.
func GetPublicKey(wif string) (out []byte, err error) {
	var w *btcutil.WIF
	w, err = btcutil.DecodeWIF(wif)
	if slog.Check(err) {
		return nil, errors.Wrap(err, "failed to decode WIF")
	}
	out = w.PrivKey.PubKey().SerializeCompressed()
	return
}
