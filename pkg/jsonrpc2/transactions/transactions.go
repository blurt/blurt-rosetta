package transactions

import (
	// Stdlib
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"gitlab.com/stalker.loki/slog"

	// RPC
	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/types"

	// Vendor
	"github.com/pkg/errors"
)

// RefBlockNum function returns blockNumber
func RefBlockNum(blockNumber uint32) types.UInt16 {
	return types.UInt16(blockNumber)
}

// RefBlockPrefix function returns block prefix
func RefBlockPrefix(blockID string) (i types.UInt32, err error) {
	// Block ID is hex-encoded.
	var rawBlockID []byte
	if rawBlockID, err = hex.DecodeString(blockID); slog.Check(err) {
		err = errors.Wrapf(err, "networkbroadcast: failed to decode block ID: %v", blockID)
		return
	}
	// Raw prefix = raw block ID [4:8].
	// Make sure we don't trigger a slice bounds out of range panic.
	if len(rawBlockID) < 8 {
		err = errors.Errorf("networkbroadcast: invalid block ID: %v", blockID)
		return
	}
	rawPrefix := rawBlockID[4:8]
	// Decode the prefix.
	var prefix uint32
	if err = binary.Read(bytes.NewReader(rawPrefix), binary.LittleEndian, &prefix); slog.Check(err) {
		err = errors.Wrapf(err, "networkbroadcast: failed to read block prefix: %v", rawPrefix)
		return
	}
	// Done, return the prefix.
	i = types.UInt32(prefix)
	return
}
