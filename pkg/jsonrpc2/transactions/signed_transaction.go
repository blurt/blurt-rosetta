// +build !nosigning

package transactions

import (
	// Stdlib
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"gitlab.com/stalker.loki/slog"
	"time"

	// RPC
	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/encoding/transaction"
	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/types"

	// Vendor
	"github.com/pkg/errors"
)

// SignedTransaction structure of a signed transaction
type SignedTransaction struct {
	*types.Transaction
}

// NewSignedTransaction initialization of a new signed transaction
func NewSignedTransaction(tx *types.Transaction) *SignedTransaction {
	if tx.Expiration == nil {
		expiration := time.Now().Add(30 * time.Second).UTC()
		tx.Expiration = &types.Time{Time: &expiration}
	}
	return &SignedTransaction{tx}
}

// Serialize function serializes a transaction
func (tx *SignedTransaction) Serialize() (out []byte, err error) {
	var b bytes.Buffer
	encoder := transaction.NewEncoder(&b)
	if err = encoder.Encode(tx.Transaction); slog.Check(err) {
		return
	}
	return b.Bytes(), nil
}

// Digest function that returns a digest from a serialized transaction
func (tx *SignedTransaction) Digest(chain string) (rawTx []byte, err error) {
	var msgBuffer bytes.Buffer
	// Write the chain ID.
	var rawChainID []byte
	rawChainID, err = hex.DecodeString(chain)
	if slog.Check(err) {
		err = errors.Wrapf(err, "failed to decode chain ID: %v", chain)
		return
	}
	if _, err = msgBuffer.Write(rawChainID); slog.Check(err) {
		err = errors.Wrap(err, "failed to write chain ID")
		return
	}
	// Write the serialized transaction.
	rawTx, err = tx.Serialize()
	if slog.Check(err) {
		return nil, err
	}
	if _, err = msgBuffer.Write(rawTx); slog.Check(err) {
		err = errors.Wrap(err, "failed to write serialized transaction")
		return
	}
	// Compute the digest.
	digest := sha256.Sum256(msgBuffer.Bytes())
	return digest[:], nil
}

// Sign function directly generating transaction signature
func (tx *SignedTransaction) Sign(privKeys [][]byte, chain string) (err error) {
	var buf bytes.Buffer
	var chainID, txRaw []byte
	if chainID, err = hex.DecodeString(chain); err != nil {
		return
	}
	if txRaw, err = tx.Serialize(); slog.Check(err) {
		return
	}
	// TODO: shouldn't these functions terminate on failure?
	if _, err := buf.Write(chainID); slog.Check(err) {
	}
	if _, err := buf.Write(txRaw); slog.Check(err) {
	}
	data := buf.Bytes()
	// msg_sha := crypto.Sha256(buf.Bytes())
	var sigHex []string
	for _, privB := range privKeys {
		var sigBytes []byte
		if sigBytes, err = tx.SignSingle(privB, data); slog.Check(err) {
			return
		}
		sigHex = append(sigHex, hex.EncodeToString(sigBytes))
	}
	tx.Transaction.Signatures = sigHex
	return
}
