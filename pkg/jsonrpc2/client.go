package jsonrpc2

import (
	"errors"
	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/api"
	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/transports"
	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/transports/http"
	"gitlab.com/stalker.loki/slog"
	"net/url"
)

var (
	ErrInitializeTransport = errors.New("failed to initialize transport")
)

// Client can be used to access Blurt remote APIs.
// There is a public field for every Blurt API available,
// e.g. Client.Database corresponds to database_api.
type Client struct {
	cc            transports.CallCloser
	chainID       string
	AsyncProtocol bool
	// Database represents database_api.
	API *api.API
	// Current keys for operations
	CurrentKeys *Keys
}

// NewClient creates a new RPC client that use the given CallCloser internally.
// Initialize only server present API. Absent API initialized as nil value.
func NewClient(s string) (cl *Client, err error) {
	// Parse URL
	var u *url.URL
	if u, err = url.Parse(s); slog.Check(err) {
		return
	}
	// Initializing Transport
	var call transports.CallCloser
	switch u.Scheme {
	case "wss", "ws":
		panic("websockets are not supported because the server doesn't really support it and there is no notification" +
			" types") // TODO: this may not be true entirely though polling is required to run them
	case "https", "http":
		if call, err = http.NewTransport(s); slog.Check(err) {
			return
		}
	default:
		return nil, ErrInitializeTransport
	}
	cl = &Client{cc: call}
	cl.AsyncProtocol = false
	cl.API = api.NewAPI(cl.cc)
	var chainID *api.Config
	if chainID, err = cl.API.GetConfig(); slog.Check(err) {
		return
	}
	cl.chainID = chainID.ChainID
	return
}

// Close should be used to close the client when no longer needed.
// It simply calls Close() on the underlying CallCloser.
func (client *Client) Close() error {
	return client.cc.Close()
}
