package api

import (
	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/types"
	"gitlab.com/stalker.loki/slog"
)

// network_broadcast_api

// BroadcastTransaction api request broadcast_transaction
func (api *API) BroadcastTransaction(tx *types.Transaction) (err error) {
	if err = api.call("network_broadcast_api", "broadcast_transaction",
		[]interface{}{tx}, nil); slog.Check(err) {
	}
	return
}

// BroadcastTransactionSynchronous api request broadcast_transaction_synchronous
func (api *API) BroadcastTransactionSynchronous(tx *types.Transaction) (resp *BroadcastResponse, err error) {
	if err = api.call("network_broadcast_api", "broadcast_transaction_synchronous",
		[]interface{}{tx}, resp); slog.Check(err) {
	}
	return
}
