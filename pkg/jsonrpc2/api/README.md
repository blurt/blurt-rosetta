# API

This package adds support for `blurt api`.

## State
| | | |
|:---                           | :--- | :--- |
| **API**                           | **Command Name**                          | **Status**    |
| account_by_key                |
|                               | get_key_references                    |           |
| account_history               |
|                               | get_account_history                   |           |
| committee_api                 |
|                               | get_committee_request                 |           |
|                               | get_committee_request_votes           |           |
|                               | get_committee_requests_list           |           |
| database_api                  |
|                               | get_account_count                     |           |
|                               | get_accounts                          |           |
|                               | get_block                             |           |
|                               | get_block_header                      |           |
|                               | get_chain_properties                  |           |
|                               | get_config                            |           |
|                               | get_database_info                     |           |
|                               | get_dynamic_global_properties         |           |
|                               | get_escrow                            |           |
|                               | get_expiring_vesting_delegations      |           |
|                               | get_hardfork_version                  |           |
|                               | get_next_scheduled_hardfork           |           |
|                               | get_owner_history                     |           |
|                               | get_potential_signatures              |           |
|                               | get_proposed_transaction              |           |
|                               | get_recovery_request                  |           |
|                               | get_required_signatures               |           |
|                               | get_transaction_hex                   |           |
|                               | get_vesting_delegations               |           |
|                               | get_withdraw_routes                   |           |
|                               | lookup_account_names                  |           |
|                               | lookup_accounts                       |           |
|                               | verify_account_authority              |           |
|                               | verify_authority                      |           |
| follow                        |
|                               | get_blog                              |           |
|                               | get_blog_authors                      |           |
|                               | get_blog_entries                      |           |
|                               | get_feed                              |           |
|                               | get_feed_entries                      |           |
|                               | get_follow_count                      |           |
|                               | get_followers                         |           |
|                               | get_following                         |           |
|                               | get_reblogged_by                      |           |
| invite_api                    |
|                               | get_invites_list                      |           |           
|                               | get_invite_by_id                      |           |           
|                               | get_invite_by_key                     |           |           
| network_broadcast_api         |
|                               | broadcast_block                       |           |
|                               | broadcast_transaction                 |           |
|                               | broadcast_transaction_synchronous     |           |
|                               | broadcast_transaction_with_callback   |           |
| operation_history             |
|                               | get_ops_in_block                      |           |
|                               | get_transaction                       |           |
| social_network                |
|                               | get_account_votes                     |           |
|                               | get_active_votes                      |           |
|                               | get_all_content_replies               |           |
|                               | get_content                           |           |
|                               | get_content_replies                   |           |
|                               | get_replies_by_last_update            |           |
| tags                          |
|                               | get_discussions_by_active             |           |
|                               | get_discussions_by_author_before_date |           |
|                               | get_discussions_by_blog               |           |
|                               | get_discussions_by_cashout            |           |
|                               | get_discussions_by_children           |           |
|                               | get_discussions_by_contents           |           |
|                               | get_discussions_by_created            |           |
|                               | get_discussions_by_feed               |           |
|                               | get_discussions_by_hot                |           |
|                               | get_discussions_by_payout             |           |
|                               | get_discussions_by_trending           |           |
|                               | get_discussions_by_votes              |           |
|                               | get_languages                         |           |
|                               | get_tags_used_by_author               |           |
|                               | get_trending_tags                     |           |
| witness_api                   |                                       |           |
|                               | get_active_witnesses                  |           |
|                               | get_miner_queue                       |           |
|                               | get_witness_by_account                |           |
|                               | get_witness_count                     |           |
|                               | get_witness_schedule                  |           |
|                               | get_witnesses                         |           |
|                               | get_witnesses_by_vote                 |           |
|                               | lookup_witness_accounts               |           |

## License

MIT, see the `LICENSE` file.
