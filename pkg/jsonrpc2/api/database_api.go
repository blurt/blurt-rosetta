package api

import (
	"encoding/json"
	"gitlab.com/stalker.loki/slog"

	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/transports"
	//_ "gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/types" // TODO: being such a large folder not sure what this invokes
)

// GetConfig api request get_config
func (api *API) GetConfig() (resp *Config, err error) {
	if err = api.call("database_api", "get_config",
		transports.EmptyParams, &resp); slog.Check(err) {
	}
	return
}

// GetDynamicGlobalProperties api request get_dynamic_global_properties
func (api *API) GetDynamicGlobalProperties() (resp *DynamicGlobalProperties, err error) {
	if err = api.call("database_api", "get_dynamic_global_properties",
		transports.EmptyParams, &resp); slog.Check(err) {
	}
	return
}

// GetBlock api request get_block
func (api *API) GetBlock(blockNum uint32) (resp *Block, err error) {
	if err = api.call("database_api", "get_block",
		[]uint32{blockNum}, &resp); slog.Check(err) {
	}
	resp.Number = blockNum
	return
}

// GetBlockHeader api request get_block_header
func (api *API) GetBlockHeader(blockNum uint32) (resp *BlockHeader, err error) {
	if err = api.call("database_api", "get_block_header",
		[]uint32{blockNum}, &resp); slog.Check(err) {
	}
	resp.Number = blockNum
	return
}

// Set callback to invoke as soon as a new block is applied
func (api *API) SetBlockAppliedCallback(notice func(header *BlockHeader, error error)) (err error) {
	err = api.setCallback("database_api", "set_block_applied_callback", func(raw json.RawMessage) {
		var header []BlockHeader
		if err = json.Unmarshal(raw, &header); slog.Check(err) {
			notice(nil, err)
		}
		for _, b := range header {
			notice(&b, nil)
		}
	})
	return
}
