package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2"
	"gitlab.com/stalker.loki/slog"
	"os"
)

type JSONParams struct {
	JSONRPC string      `json:"jsonrpc"`
	Method  string      `json:"method"`
	Params  interface{} `json:"params"`
	ID      string      `json:"id"`
}

func main() {
	switch {
	case len(os.Args) != 5 || (len(os.Args) == 2 && os.Args[1] == "help"):
		fmt.Print(`blurtq - simple query CLI for blurt blockchain

usage:

	blurtq <http blurtd address> <api> <method> '<json formatted parameters>'

notes:
	
	Address does not require scheme prefix as there is only http enabled for local access, eg: localhost:8091
	
	Note that the json parameters MUST be single-quoted in order to retain the contained double quotes eg '[["username"]]'

`)
	case len(os.Args) == 5:
		fmt.Println("received parameters:")
		fmt.Println()
		fmt.Println("\tendpoint:\thttp://" + os.Args[1])
		fmt.Println("\tapi:\t\t" + os.Args[2])
		fmt.Println("\tmethod:\t\t" + os.Args[3])
		params := os.Args[4]
		var intermediate interface{}
		if err := json.Unmarshal([]byte(params), &intermediate); slog.Check(err) {
			slog.Fatal("json parameter is not correctly formatted")
		}
		if o, err := json.MarshalIndent(intermediate, "\t", "\t"); slog.Check(err) {
			slog.Fatal("failed to marshal parameters")
		} else {
			params = string(o)
		}
		fmt.Println("\tparameters:\n\n\t" + params)
		fmt.Println()
		// encapsulate parameters into JSONRPC2 wrapper
		jsonParameters := JSONParams{
			JSONRPC: "2.0",
			Method:  os.Args[2] + "." + os.Args[3],
			Params:  intermediate,
			ID:      "1",
		}
		if o, err := json.Marshal(&jsonParameters); slog.Check(err) {
			slog.Fatal("error marshalling jsonrpc2 request")
		} else {
			fmt.Println(string(o))
		}
		if cl, err := jsonrpc2.NewClient("http://" + os.Args[1]); slog.Check(err) {
			slog.Fatal("error opening client connection to http://"+os.Args[1], err)
		} else {
			_ = cl
		}
	}
}
