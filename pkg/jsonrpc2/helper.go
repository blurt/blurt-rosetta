package jsonrpc2

import (
	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/api"
	"gitlab.com/stalker.loki/slog"
	"strconv"

	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/transactions"
	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/types"
)

// SetAsset returns data of type Asset
func SetAsset(amount float64, symbol string) *types.Asset {
	return &types.Asset{Amount: amount, Symbol: symbol}
}

// PerMvest returns the ratio of TotalVestingFund to TotalVestingShares.
func (client *Client) PerMvest() (spm float64, err error) {
	var dgp *api.DynamicGlobalProperties
	if dgp, err = client.API.GetDynamicGlobalProperties(); slog.Check(err) {
		return
	}
	totFund := dgp.TotalVestingFund.Amount
	totShares := dgp.TotalVestingShares.Amount
	s := (totFund / totShares) * 1000000
	str := strconv.FormatFloat(s, 'f', 3, 64)
	if spm, err = strconv.ParseFloat(str, 64); slog.Check(err) {
	}
	return
}

// JSONTrxString generate Trx to String
func JSONTrxString(v *transactions.SignedTransaction) (s string, err error) {
	var ans []byte
	if ans, err = types.JSONMarshal(v); slog.Check(err) {
	} else {
		s = string(ans)
	}
	return
}

// JSONOpString generate Operations to String
func JSONOpString(v []types.Operation) (s string, err error) {
	var tx types.Operations
	tx = append(tx, v...)
	var ans []byte
	if ans, err = types.JSONMarshal(tx); !slog.Check(err) {
		s = string(ans)
	}
	return
}
