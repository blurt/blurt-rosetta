package jsonrpc2

import (
	"gitlab.com/stalker.loki/slog"
	"time"

	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/api"
	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/transactions"
	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2/types"
)

// BResp of response when sending a transaction.
type BResp struct {
	ID       string
	BlockNum int32
	TrxNum   int32
	Expired  bool
	JSONTrx  string
}

// SendTrx generates and sends an array of transactions to Blurt.
func (client *Client) SendTrx(username string, strx []types.Operation) (bresp *BResp, err error) {
	bresp = &BResp{}
	// Getting the necessary parameters
	var props *api.DynamicGlobalProperties
	props, err = client.API.GetDynamicGlobalProperties()
	if err != nil {
		return nil, err
	}
	// Creating a Transaction
	var refBlockPrefix types.UInt32
	if refBlockPrefix, err = transactions.RefBlockPrefix(props.HeadBlockID); slog.Check(err) {
		return
	}
	tx := transactions.NewSignedTransaction(&types.Transaction{
		RefBlockNum:    transactions.RefBlockNum(props.HeadBlockNumber),
		RefBlockPrefix: refBlockPrefix,
	})
	// Adding Operations to a Transaction
	for _, val := range strx {
		tx.PushOperation(val)
	}
	expTime := time.Now().Add(59 * time.Minute).UTC()
	tm := types.Time{
		Time: &expTime,
	}
	tx.Expiration = &tm
	// Obtain the key required for signing
	var privKeys [][]byte
	if privKeys, err = client.SigningKeys(strx[0]); slog.Check(err) {
		return
	}
	// Sign the transaction
	if err = tx.Sign(privKeys, client.chainID); err != nil {
		return
	}
	// Sending a transaction
	var resp *api.BroadcastResponse
	if client.AsyncProtocol {
		err = client.API.BroadcastTransaction(tx.Transaction)
	} else {
		resp, err = client.API.BroadcastTransactionSynchronous(tx.Transaction)
	}
	bresp.JSONTrx, _ = JSONTrxString(tx)
	if err != nil {
		return bresp, err
	}
	if resp != nil && !client.AsyncProtocol {
		bresp.ID, bresp.BlockNum, bresp.TrxNum, bresp.Expired =
			resp.ID, resp.BlockNum, resp.TrxNum, resp.Expired
	}
	return
}

func (client *Client) GetTrx(strx []types.Operation) (tx *types.Transaction, err error) {
	// Getting the necessary parameters
	var props *api.DynamicGlobalProperties
	if props, err = client.API.GetDynamicGlobalProperties(); slog.Check(err) {
		return
	}
	// Creating a Transaction
	var refBlockPrefix types.UInt32
	if refBlockPrefix, err = transactions.RefBlockPrefix(props.HeadBlockID); slog.Check(err) {
		return
	}
	tx = &types.Transaction{
		RefBlockNum:    transactions.RefBlockNum(props.HeadBlockNumber),
		RefBlockPrefix: refBlockPrefix,
	}
	// Adding Operations to a Transaction
	for _, val := range strx {
		tx.PushOperation(val)
	}
	expTime := time.Now().Add(59 * time.Minute).UTC()
	tm := types.Time{
		Time: &expTime,
	}
	tx.Expiration = &tm
	return tx, nil
}
