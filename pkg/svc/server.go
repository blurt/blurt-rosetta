package svc

import (
	"context"
	"github.com/coinbase/rosetta-sdk-go/asserter"
	"github.com/coinbase/rosetta-sdk-go/server"
	"github.com/coinbase/rosetta-sdk-go/types"
	"gitlab.com/stalker.loki/slog"

	"gitlab.com/blurt/blurt-rosetta/pkg/jsonrpc2"
)

type Server struct {
	Ctx      context.Context
	Client   *jsonrpc2.Client
	Network  *types.NetworkIdentifier
	Asserter *asserter.Asserter
}

var (
	// ensure all implementations exist and the type implements the interface
	_ server.NetworkAPIServicer      = (*Server)(nil)
	_ server.BlockAPIServicer        = (*Server)(nil)
	_ server.AccountAPIServicer      = (*Server)(nil)
	_ server.ConstructionAPIServicer = (*Server)(nil)
)

func New(ctx context.Context, endpoint string) (server *Server, err error) {
	network := &types.NetworkIdentifier{
		Blockchain: "blurt",
		Network:    "main",
	}
	server = &Server{
		Ctx:     ctx,
		Network: network,
	}
	// start up rpc client and ensure endpoint exists and returns correct responses
	// this is not a persistent connection as blurtd does not support sockets so it just ensures
	// there is a reachable endpoint and stores a handle
	slog.Info("Connecting to Blurt node running at", endpoint)
	if server.Client, err = jsonrpc2.NewClient(endpoint); slog.Check(err) {
		return
	}
	allTypes := []string{"debit", "credit"}
	if server.Asserter, err = asserter.NewServer(allTypes, true, []*types.NetworkIdentifier{network}); slog.Check(err) {
	}
	return
}

func (s *Server) Run() (err error) {
	slog.Debug("starting blurt-rosetta endpoint")
	<-s.Ctx.Done()
	slog.Debug("rosetta endpoint finished")
	return
}
