package svc

import (
	"context"

	"github.com/coinbase/rosetta-sdk-go/types"
)

func (s *Server) ConstructionCombine(ctx context.Context,
	request *types.ConstructionCombineRequest) (*types.ConstructionCombineResponse,
	*types.Error) {
	panic("implement me")
}

func (s *Server) ConstructionDerive(ctx context.Context,
	request *types.ConstructionDeriveRequest) (*types.ConstructionDeriveResponse,
	*types.Error) {
	panic("implement me")
}

func (s *Server) ConstructionHash(ctx context.Context,
	request *types.ConstructionHashRequest) (*types.TransactionIdentifierResponse,
	*types.Error) {
	panic("implement me")
}

func (s *Server) ConstructionMetadata(ctx context.Context,
	request *types.ConstructionMetadataRequest) (
	*types.ConstructionMetadataResponse, *types.Error) {
	panic("implement me")
}

func (s *Server) ConstructionParse(ctx context.Context,
	request *types.ConstructionParseRequest) (*types.ConstructionParseResponse,
	*types.Error) {
	panic("implement me")
}

func (s *Server) ConstructionPayloads(ctx context.Context,
	request *types.ConstructionPayloadsRequest) (
	*types.ConstructionPayloadsResponse, *types.Error) {
	panic("implement me")
}

func (s *Server) ConstructionPreprocess(ctx context.Context,
	request *types.ConstructionPreprocessRequest) (
	*types.ConstructionPreprocessResponse, *types.Error) {
	panic("implement me")
}

func (s *Server) ConstructionSubmit(ctx context.Context,
	request *types.ConstructionSubmitRequest) (*types.TransactionIdentifierResponse,
	*types.Error) {
	panic("implement me")
}
