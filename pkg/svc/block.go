package svc

import (
	"context"

	"github.com/coinbase/rosetta-sdk-go/types"
)

func (s *Server) Block(ctx context.Context, request *types.BlockRequest) (
	*types.BlockResponse, *types.Error) {
	panic("implement me")
}

func (s *Server) BlockTransaction(ctx context.Context,
	request *types.BlockTransactionRequest) (*types.BlockTransactionResponse,
	*types.Error) {
	panic("implement me")
}
