package svc

import (
	"context"

	"github.com/coinbase/rosetta-sdk-go/types"
)

func (s *Server) AccountBalance(ctx context.Context,
	request *types.AccountBalanceRequest) (*types.AccountBalanceResponse, *types.Error) {
	panic("implement me")
}
